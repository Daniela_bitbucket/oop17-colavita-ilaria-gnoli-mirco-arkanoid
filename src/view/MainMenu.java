package view;

import java.awt.Toolkit;

import javafx.stage.Stage;

/**
 * Classe che rappresenta la view di partenza, il menù principale. Estende Stage.
 */
public class MainMenu extends Stage {
    /**
     * Larghezza della finestra, dipendente dalle dimensioni dello schermo.
     */
    public static final int MAIN_WINDOW_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width / 3;

    /**
     * Altezza della finestra, dipendente dalle dimensioni dello schermo.
     */
    public static final int MAIN_WINDOW_HEIGHT = (int) (Toolkit.getDefaultToolkit().getScreenSize().height * 0.8);

    /**
     * 
     */
    public MainMenu() {
        super();
        this.setTitle("Arkanoid menu");
        this.setWidth(MAIN_WINDOW_WIDTH);
        this.setHeight(MAIN_WINDOW_HEIGHT);
        this.setResizable(false);

        this.setScene(new DefaultScene(this));
        this.show();
    }
}
