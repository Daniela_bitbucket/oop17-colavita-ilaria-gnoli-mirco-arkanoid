package view;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.ModelCostant;
/**
 * Pannello che estende {@link HBox}, che visualizza punteggi e vite.
 */
public class ScoreAndLivesPanel extends HBox {

    private final Font font = Font.loadFont(this.getClass().getClassLoader().getResourceAsStream("Font/UnicaOne-Regular.ttf"), 26);
    private static final Insets PADDING = new Insets(10);

    private final Label scoreL;
    private final Label livesL;

    /**
     *
     */
    public ScoreAndLivesPanel() {
        super();

        scoreL = new MyLabel("Score: 0");
        livesL = new MyLabel("Lives: 3");
        this.getChildren().addAll(scoreL, livesL);
    }

    /**
     * Metodo di aggiornamento del pannello.
     * @param score - nuovo punteggio
     * @param lives - vite rimanenti
     */
    public void refresh(final int score, final int lives) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                scoreL.setText("Score: " + score);
                livesL.setText("Lives: " + lives);
            }
        });
    }

    private class MyLabel extends Label {

        MyLabel(final String text) {
            super(text);
            this.setFont(font);
            this.setPadding(PADDING);
            this.setTextFill(Color.DARKSLATEBLUE);
            this.setPrefWidth(ModelCostant.WORLD_WIDTH / 2);
            this.setAlignment(Pos.CENTER);
        }
    }
}
