package controller;

import java.util.List;

import javafx.application.Platform;
import javafx.scene.input.KeyCode;
import model.BasicGame;
import model.ClassicGame;
import model.GameMode;
import model.IBasicGame;
import model.SurvivalGame;
import model.IBasicGame.GameStatus;
import model.entities.Entity;
import view.EndGameView;
import view.GameView;
import view.MainMenu;
/**
 * Classe adibita al coordinamento fra view e model.
 */
public class Controller implements IController, Runnable, ISubject {

    private static final int FRAME_PER_SECOND = 20;
    private static final int MILLISEC_TO_REFRESH = 1000 / FRAME_PER_SECOND;

    private IObserver obs;
    private IBasicGame game;
    private final GameView view;
    private final GameMode mode;
    /**
     * 
     * @param mode - modalita' di gioco scelta
     */
    public Controller(final GameMode mode) {
        this.view = new GameView();
        this.setInputHandler();
        addObserver(new IObserver() {

            @Override
            public void updateScoreLives(final int score, final int lives) {
                view.getInfoPanel().refresh(score, lives);
            }

            @Override
            public  void updateAllEntities(final List<Entity> entities) {
                view.getArena().refresh(entities);
            }
        });

        this.mode = mode;
    }

    @Override
    public final void run() {
        switch (mode) {
        case CLASSIC:
            this.game = new BasicGame();
            break;
        case STANDARD:
            this.game = new ClassicGame();
            break;
        case SURVIVAL:
            this.game = new SurvivalGame();
            break;
        default:
            System.out.println("Errore. Non dovrebbe mai entrare qui.");
            break;
        }

        IBasicGame.GameStatus status = null;
        while (status != IBasicGame.GameStatus.WIN && status != IBasicGame.GameStatus.LOST) { //
            status = this.game.getStatus();

            switch (status) {
            case START:
                this.game.initOnStart();
                obs.updateAllEntities(this.game.getAllEntities());
                break;
            case RUNNING:
                this.game.updateModel();
                obs.updateAllEntities(this.game.getAllEntities());
                obs.updateScoreLives(this.game.getScore(), this.game.getLives());
                break;
            case PAUSE:
                this.obs.updateAllEntities(this.game.getAllEntities()); //utile per riaggiornare quando i PU finiscono ed il gioco è in pausa.
                break;
            default: //win or lost
                break;
            }

            try {
                Thread.sleep(MILLISEC_TO_REFRESH);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                doAtEndGame();
            }
        });
    }

    /**
     * Metodo utilizzato per creare una EndGameView.
     */
    private void doAtEndGame() {
        new EndGameView(this);
    }

    /**
     * Metodo di salvataggio del punteggio.
     * @param name - nome inserito in fase di salvataggio.
     */
    public void saveScore(final String name) {
        HighscoreManager.getInstance().addScore(name, game.getScore(), this.mode);
        HighscoreManager.getInstance().saveAllScores();
        openMenu();
    }

    @Override
    public final void openMenu() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                view.close();
                new MainMenu();
            }
        });
    }

    @Override
    public final GameView getView() {
        return this.view;
    }

    @Override
    public final IBasicGame getGame() {
        return this.game;
    }

    /**
     * Metodo che imposta i comandi ottenibili da tastiera.
     */
    private void setInputHandler() {
        this.view.getScene().setOnKeyPressed(e -> {
            if (this.game.getStatus() != GameStatus.PAUSE || e.getCode().equals(KeyCode.SPACE)) {
                switch (e.getCode()) {
                case LEFT:
                    this.game.getBar().moveLeft();
                    break;
                case SPACE:
                    this.game.setPause();
                    break;
                case RIGHT:
                    this.game.getBar().moveRight();
                    break;
                default:
                    System.out.println("Tasto premuto: " + e.getCode() + ". Usa le frecce direzionali per muoverti, Spazio per pausa.");
                    break;
                }
            }
        });
    }

    @Override
    public final void addObserver(final IObserver obs) {
        this.obs = obs;
    }
}
