package model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import model.entities.Brick;
import utility.MapUtils;

/**
 * Classe concreta che modella il gioco in modalit� SURVIVAL. Concretizza {@link AbstractGame}.
 * @see GameMode
 */
public class SurvivalGame extends AbstractAdvancedGame {

    @Override
    protected final void doIfBricksRowIsEmpty(final int row, final Brick template) {
        final Map<Integer, List<Brick>> newMap = new HashMap<>(this.getBricks());
        newMap.remove(row);
        MapUtils.collapse(newMap);
        newMap.put(newMap.keySet().stream().reduce((x, y) -> Math.min(x, y)).get() - 1, getFactory().randomBrickRow());

        int count = 0;
        for (final Integer key : new TreeSet<>(newMap.keySet())) {
            for (final Brick brick : newMap.get(key)) {
                brick.setPosition(brick.getPosition().getKey(), count + count * brick.getHeight());
            }
            count++;
        }

        this.setBricks(newMap);
    }
}
