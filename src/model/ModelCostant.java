package model;

/**
 * Classe pubblica per tenere traccia di tutte le impostazioni di gioco del modello.
 */
public final class ModelCostant {

    /**
     * Larghezza dell'arena.
     */
    public static final int WORLD_WIDTH = 720;

    /**
     * Altezza dell'arena.
     */
    public static final int WORLD_HEIGHT = 610;

    /**
     * Numero di righe di mattoncini.
     */
    public static final int NUMBER_OF_ROW = 5;

    /**
     * Numero di mattoncini in una riga.
     */
    public static final int NUMBER_OF_COLUMN = 12;

    /**
     * Distanza della barra dalla parete.
     */
    public static final int DEFAULT_OFFSET_FROM_WALL = 9;

    /**
     * Arrotondamento della barra alle estremità.
     */
    public static final int DEFAULT_ARC = 8;

    private ModelCostant() { }
}
