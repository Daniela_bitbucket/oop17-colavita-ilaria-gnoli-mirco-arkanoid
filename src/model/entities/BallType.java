package model.entities;

import java.util.Optional;

import javafx.geometry.HorizontalDirection;
import javafx.geometry.Side;
import javafx.geometry.VerticalDirection;
import javafx.util.Pair;
import utility.CollisionUtility;

/**
 * Semplice enumerazione che definisce le diverse tipologie di pallina e, per ciascuna, un tipo di rimbalzo.
 */
public enum BallType {
    /**
     * Pallina standard, rimbalzo standard.
     */ 
    STANDARD_BALL {
        /**
         * {@inheritDoc}
         */
        public void bounce(final Ball ball, final Entity ent) {
            if (ent instanceof Wall || ent instanceof Brick) {
                standardBounce(ball, ent);
            } else if (ent instanceof Bar) {
                barBounce(ball, (Bar) ent);
            }
        }
    },

    /**
     * Pallina di fuoco, non rimbalza contro i {@link Brick} che si possono rompere.
     *
     */
    FIRE_BALL {
        /**
         * {@inheritDoc}
         */
        public void bounce(final Ball ball, final Entity ent) {
            if (ent instanceof Wall) { // || (ent instanceof Brick) && ((Brick) ent).isIndistruttibile())) { //controllo se ci dovessero essere i mattoni indistruttibili
                standardBounce(ball, ent);
            } else if (ent instanceof Bar) {
                barBounce(ball, (Bar) ent);
            }
        }
    };

    /**
     * Metodo utilizzato per il rimbalzo della pallina.
     * 
     * @param ball - {@link Ball} che rimbalza
     * @param ent - {@link Entity} contro cui rimbalza la pallina
     */
    public abstract void bounce(Ball ball, Entity ent);

    /**
     * Rimbalzo della pallina contro la barra.
     * Se cade nel terzo pi� a destra, rimbalza verso destra, viceversa a sinistra.
     * Se cade nel terzo centrale, rimbalza in maniera standard come si ci aspetta.
     *
     * @param ball
     * @param bar
     */
    private static void barBounce(final Ball ball, final Bar bar) {
        ball.setPosition(ball.getPosition().getKey(), bar.getMinY() - ball.getRadius());

        if (ball.getPosition().getKey() < bar.getMinX() + bar.getLength() / 3 && ball.getDirection().getKey().isPresent() && ball.getDirection().getKey().get() == HorizontalDirection.RIGHT) {
            ball.doOnCollision(Side.RIGHT);
        } else if (ball.getPosition().getKey() > bar.getMaxX() - bar.getLength() / 3 && ball.getDirection().getKey().isPresent() && ball.getDirection().getKey().get() == HorizontalDirection.LEFT) {
            ball.doOnCollision(Side.LEFT);
        }

        ball.doOnCollision(Side.BOTTOM);
    }

    /**
     * Rimbalzo standard della {@link Ball} ball a contatto con {@link Entity} ent.
     * 
     * @param ball
     * @param ent
     */
    private static void standardBounce(final Ball ball, final Entity ent) {
        final Pair<Optional<HorizontalDirection>, Optional<VerticalDirection>> dir = ball.getDirection();

        if (dir.getKey().isPresent()) {
            if (dir.getKey().get().equals(HorizontalDirection.RIGHT)) {
                if (CollisionUtility.firstCollidedWithLeftestVerticalBound(ball, ent)) {
                    ball.doOnCollision(Side.RIGHT);
                }
            } else {
                if (CollisionUtility.firstCollidedWithRightestVerticalBound(ball, ent)) {
                    ball.doOnCollision(Side.LEFT);
                }
            }
        }

        if (dir.getValue().isPresent()) {
            if (dir.getValue().get().equals(VerticalDirection.UP)) {
                if (CollisionUtility.firstCollidedWithLowerHorizontalBound(ball, ent)) {
                    ball.setPosition(ball.getPosition().getKey(), ent.getMaxY() + ball.getRadius());
                    ball.doOnCollision(Side.TOP);
                }
            } else {
                if (CollisionUtility.firstCollidedWithTopHorizontalBound(ball, ent)) {
                    ball.setPosition(ball.getPosition().getKey(), ent.getMinY() - ball.getRadius());
                    ball.doOnCollision(Side.BOTTOM);
                }
            }
        }
    }
}
