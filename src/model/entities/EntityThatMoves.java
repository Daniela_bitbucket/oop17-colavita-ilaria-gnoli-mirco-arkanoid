package model.entities;

/**
 * Interfaccia delle entit� di gioco che hanno la possibilit� di muoversi.
 */
public interface EntityThatMoves extends Entity {

    /**
     * Aggiorna la posizione dell'entit�.
     */
    void refreshPosition();
}
