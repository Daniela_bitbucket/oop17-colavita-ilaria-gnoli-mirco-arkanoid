package model.entities;

import javafx.util.Pair;
/**
 * Projectile model. Implements {@link EntityThatMoves}.
 * 
 */
public class Projectile implements EntityThatMoves {

    /**
     * Speed of the projectile.
     */
    protected static final int SPEED = 10;
    /**
     * Width of the projectile.
     */
    protected static final int PROJ_WIDTH = 3;
    /**
     * Height of the projectile.
     */
    protected static final int PROJ_HEIGHT = 15;

    private final int width;
    private final int height;
    private Pair<Integer, Integer> position;
    private final int speed;

    /**
     * Constructor.
     * 
     * @param width
     *          the projectile width
     * @param height
     *          the projectile height
     * @param position
     *          the projectile position
     */
    Projectile(final Pair<Integer, Integer> position) {
        this.width = PROJ_WIDTH;
        this.height = PROJ_HEIGHT;
        this.position = position;
        this.speed = SPEED;
    }

    @Override
    public final int getMaxX() {
        return this.position.getKey() + this.width;
    }

    @Override
    public final int getMinX() {
        return this.position.getKey();
    }

    @Override
    public final int getMaxY() {
        return this.position.getValue() + this.height;
    }

    @Override
    public final int getMinY() {
        return this.position.getValue();
    }

    @Override
    public final Pair<Integer, Integer> getPosition() {
        return this.position;
    }

    @Override
    public final void setPosition(final int newX, final int newY) {
        this.position = new Pair<>(newX, newY);
    }

    /**
     * @return the projectile width
     */
    public int getWidth() {
        return this.width;
    }
    /**
     * @return the projectile height
     */
    public int getHeight() {
        return this.height;
    }
    /**
     * @return the projectile speed
     */
    public int getVelocity() {
        return this.speed;
    }

    @Override
    public final void refreshPosition() {
        setPosition(this.position.getKey(), this.position.getValue() - SPEED);
    }

}
