package model.entities;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Interfaccia che definisce una factory di {@link Entity}.
 */
public interface IEntityFactory {

    /**
     * @return {@link Ball} con i parametri standard.
     */
    Ball standardBall();

    /**
     * @param b - {@link Ball} da cui farne delle copie aventi stessa posizione e velocit� di partenza, ma differente direzione.
     * 
     * @return List formata da 2 Ball.
     */
    List<Ball> multipleBall(Ball b);

    /**
     * @return List di {@link Wall}, che delimitano il campo da gioco.
     */
    List<Wall> standardWalls();

    /**
     * @return List di {@link Brick} scelti in maniera random fra quelli disponibili in {@link BrickType}
     */
    List<Brick> randomBrickRow();

    /**
     * @return Map (indice riga, List di mattoni)
     */
    Map<Integer, List<Brick>> randomBrickMap();

    /**
     * @return {@link Bar}
     */
    Bar standardBar();


    /**
     * @param bar 
     * @return List of 2 {@link Projectile}
     */
    List<Projectile> pairOfProjectile(Bar bar);

    /**
     * 
     * @param brickGenerator - {@link Brick} dalla cui rottura � generato il power up
     * @return Optional({@linkPowerUp}).
     */
    Optional<PowerUp> randomPowerUp(Brick brickGenerator);
}
